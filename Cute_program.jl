### A Pluto.jl notebook ###
# v0.19.32

using Markdown
using InteractiveUtils

# ╔═╡ dd42a400-8bce-11ee-29d3-7d50ed8bbf51
using Pkg

# ╔═╡ 4f32f96a-62a6-4800-ae2a-d48e18f5ef09
Pkg.add("AMDGPU")

# ╔═╡ 2923f610-86f8-4d9f-b2f0-192308dd594d
Pkg.add("CSV")

# ╔═╡ a48f3f61-ea2d-4f55-a16e-252d406162f2
Pkg.add("DataFrames")

# ╔═╡ 895c1539-96bf-4e2e-a50e-cb2d36162954
using ROCArrays

# ╔═╡ 4b472b96-2fc5-4125-a945-f645ab832de1
using CSV, AMDGPU, DataFrames, Dates

# ╔═╡ 3f048dca-4ea1-4009-ad8a-abd006337be1
file_path = "C:/Users/Moral Katonyala/Documents/results.csv"

# ╔═╡ 6bf7b4f9-49a9-4202-9967-7f8824b74a13
# Read CSV file into a DataFrame
data = CSV.File(file_path) |> DataFrame

# ╔═╡ 46af9d48-0487-4e9b-9b81-9947d77035b2
# Display the DataFrame
println(data)

# ╔═╡ a1cdd8a7-3756-4856-9133-2ac92f5b8ec5
#Determine the date range to apply in filtering
function lf_df(first_year, last_year)
	
	first_date = Dates.Date(first_year,1,1)
	last_date = Dates.Date(last_year, 12, 31)

	#subset to cover the specified time frame

	return filter(row -> first_date <= Dates.Date(row[:date]) <= last_date, df)
end

# ╔═╡ c571fd90-00dc-4fcb-85ff-a6074586cd62
function lf_df(df, first_year, last_year)
    first_date = Dates.Date(first_year, 1, 1)
    last_date = Dates.Date(last_year, 12, 31)

    # Subset to cover the specified time frame
    return filter(row -> first_date <= Dates.Date(row[:date]) <= last_date, data)
end

# ╔═╡ 012dbae8-ae02-4d7a-a5a0-d06acdab7b17
filtered_df = lf_df(data, 2000, 2020)

# ╔═╡ Cell order:
# ╠═dd42a400-8bce-11ee-29d3-7d50ed8bbf51
# ╠═4f32f96a-62a6-4800-ae2a-d48e18f5ef09
# ╠═2923f610-86f8-4d9f-b2f0-192308dd594d
# ╠═a48f3f61-ea2d-4f55-a16e-252d406162f2
# ╠═895c1539-96bf-4e2e-a50e-cb2d36162954
# ╠═4b472b96-2fc5-4125-a945-f645ab832de1
# ╠═3f048dca-4ea1-4009-ad8a-abd006337be1
# ╠═6bf7b4f9-49a9-4202-9967-7f8824b74a13
# ╠═46af9d48-0487-4e9b-9b81-9947d77035b2
# ╠═a1cdd8a7-3756-4856-9133-2ac92f5b8ec5
# ╠═c571fd90-00dc-4fcb-85ff-a6074586cd62
# ╠═012dbae8-ae02-4d7a-a5a0-d06acdab7b17
