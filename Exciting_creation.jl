### A Pluto.jl notebook ###
# v0.19.32

using Markdown
using InteractiveUtils

# ╔═╡ 28403f10-8bb3-11ee-0e70-71ed5ac1dcd2
using Pkg

# ╔═╡ 1312581e-ab7e-47c6-997f-27eafebab365
Pkg.add("AMDGPU")

# ╔═╡ afca3fc5-0c53-49f7-a31a-5958f027cc11
Pkg.add("LinearAlgebra")

# ╔═╡ f5456a3d-1e53-4f90-80cb-bb45fedbc3e3
using AMDGPU

# ╔═╡ da9bd88f-1b47-4c1f-b66b-fcf3d917c2bb
using LinearAlgebra

# ╔═╡ 3d42c0c0-348b-485b-8cc6-a423ca83fad3
A = [1.0 2.0 3.0 4.0 5.0 6.0; 7.0 8.0 9.0 10.0 11.0 12.0; 13.0 14.0 15.0 16.0 17.0 18.0; 19.0 20.0 21.0 22.0 23.0 24.0; 25.0 26.0 27.0 28.0 29.0 30.0; 31.0 32.0 33.0 34.0 35.0 36.0]

# ╔═╡ 27b41fa4-6f54-4bdb-b663-5c9974b2ec32
B = [6.0 5.0 4.0 3.0 2.0 1.0; 12.0 11.0 10.0 9.0 8.0 7.0; 18.0 17.0 16.0 15.0 14.0 13.0; 24.0 23.0 22.0 21.0 20.0 19.0; 30.0 29.0 28.0 27.0 26.0 25.0; 36.0 35.0 34.0 33.0 32.0 31.0]

# ╔═╡ 4789560b-1b84-40b5-93db-a9cf646a3740
C = zeros(Float32, 6, 6)

# ╔═╡ 86ddecb9-95fb-46e7-9b20-bca2d847440d
d_A = ROCArray(A)

# ╔═╡ c527ed98-d671-4aae-ae1a-87fcc64ad1e0
d_B = ROCArray(B)

# ╔═╡ a5b21814-c2c4-483d-8fa4-42efa209edaa
d_C = ROCArray(C)

# ╔═╡ 6cb0a506-8f42-41c1-b6c9-f74116f4114b
@roc threads=(2, 2) blocks=(3, 3) gpu_mul!(d_C, d_A, d_B)

# ╔═╡ db1d30dc-e596-4665-879c-2f491adcc872
# ╠═╡ disabled = true
#=╠═╡
function matrix_multiplication(Z, X, Y, B)
    i, j = threadIdx().x, blockIdx().x
    
	if i <= B
		val = 0.0
			for k in 1:N
				val += X[i, k] * Y[k, j]
			end
			Z[i, j] = val
	end
	return
end
  ╠═╡ =#

# ╔═╡ 9d8fe381-7113-42a2-9bb4-073b0ac4ba25
# ╠═╡ disabled = true
#=╠═╡
function multiply(X, Y)
 	B = size(X, 1)
 	d_X = ROCArray(X)
	d_Y = ROCArray(Y)
	d_Z = ROCArray(zeros(Float32, Y, Y))

	# Define kernel launch configuration
	block_dim = (16, 16)
	grid_dim = (div(B, block_dim[1]) + 1, div(B, block_dim[2]) + 1)

	@roc threads=block_dim blocks=grid_dim matrix_multiply_kernel(d_Z, d_X, d_Y, B)

	# Transfer the result back to the CPU
	Z = Array(d_Z)

	return Z
end
  ╠═╡ =#

# ╔═╡ 0b02babf-365a-4e1a-a13a-1bade8bbb2c8
# Create and initialize the matrices A, B, and C as shared arrays
N = 1000 # matrix size


# ╔═╡ c736a41f-4349-47a0-afae-7489387d4a10


# ╔═╡ e279513d-f8a7-499d-af69-e56a12d6cfc8
B = SharedArray(Float32, (N,N),init = s -> s[localindexes(s)] = rand(length(localindexes (s))))
C = SharedArray(Float32, (N,N));

# ╔═╡ 1d3e9ef3-bbb5-4a9b-8a21-72184083ee28
# ╠═╡ disabled = true
#=╠═╡
function gpu_mul!(C, A, B)
  i =(workgroupIdx().x - 1) * workgroupDim().x + workitemIdx().x
  j =(workgroupIdx().y - 1) * workgroupDim().y + workitemIdx().y
  if i <= size(C, 1) && j <= size(C, 2)
    C[i, j] = dot(view(A, i, :), view(B, :, j))
  end
  return
end
  ╠═╡ =#

# ╔═╡ ca52f0bd-fdf3-4087-8027-9434dce95b9d
function gpu_mul!(C, A, B)
    i, j = threadIdx().x + (blockIdx().x - 1) * blockDim().x, threadIdx().y + (blockIdx().y - 1) * blockDim().y

    if i <= size(C, 1) && j <= size(C, 2)
        C[i, j] = dot(view(A, i, :), view(B, :, j))
    end

    return
end

# ╔═╡ Cell order:
# ╠═28403f10-8bb3-11ee-0e70-71ed5ac1dcd2
# ╠═1312581e-ab7e-47c6-997f-27eafebab365
# ╠═afca3fc5-0c53-49f7-a31a-5958f027cc11
# ╠═f5456a3d-1e53-4f90-80cb-bb45fedbc3e3
# ╠═da9bd88f-1b47-4c1f-b66b-fcf3d917c2bb
# ╠═ca52f0bd-fdf3-4087-8027-9434dce95b9d
# ╠═3d42c0c0-348b-485b-8cc6-a423ca83fad3
# ╠═27b41fa4-6f54-4bdb-b663-5c9974b2ec32
# ╠═4789560b-1b84-40b5-93db-a9cf646a3740
# ╠═86ddecb9-95fb-46e7-9b20-bca2d847440d
# ╠═c527ed98-d671-4aae-ae1a-87fcc64ad1e0
# ╠═a5b21814-c2c4-483d-8fa4-42efa209edaa
# ╠═6cb0a506-8f42-41c1-b6c9-f74116f4114b
# ╠═db1d30dc-e596-4665-879c-2f491adcc872
# ╠═9d8fe381-7113-42a2-9bb4-073b0ac4ba25
# ╠═1d3e9ef3-bbb5-4a9b-8a21-72184083ee28
# ╠═0b02babf-365a-4e1a-a13a-1bade8bbb2c8
# ╠═c736a41f-4349-47a0-afae-7489387d4a10
# ╠═e279513d-f8a7-499d-af69-e56a12d6cfc8
